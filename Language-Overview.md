# Intro to programming (in Javascript)
Javascript is a computer programming language.  What is a programming language?  
Well, it's basically the vocabulary and rules that are used to write instructions for a computer.

Javascript has been around for over twenty years, and has an interesting history.  The important thing
to know is that there is a standard for Javascript and it is called ECMAScript.  There is a technical 
committee, TC39, responsible for maintaining the language.  At the time of this writing, TC39 releases
an official update (specification) to the language once a year.

Whew, now its time for a little example. You can try this now if you are on a computer, but not a mobile phone. **Type** this code in the URL of your browser (do not copy and paste or the special word *javascript* may not be output):

```javascript
javascript: alert('Hello')
```

You should see your message appear!

Browser's understand javascript!  And you just ran some code. Stepping back, when we say that we are
writing instructions for the computer, we actually mean writing instructions for whatever understands
or "interprets" javascript, in this case the browser.  Browsers have a "javascript engine" that is used
to understand or "interpret" javascript.  Node.js (sometimes referred to as node) is another tool 
that can run your javascript code.  It's important to know that each tool, the browser or node,
is responsible for following the standard, but they may not support every feature, and may add their own.  And since there are many versions of browsers and of the standard, you have to keep this in mind when writing your code.

For simplicity, this tutorial will use Node.js so that we can easily choose the exact environment that will run our code.